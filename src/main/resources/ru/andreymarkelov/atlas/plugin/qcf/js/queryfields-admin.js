(function($) {
    $(document).ready(function() {
        initJqlFieldDelay('reportJqlCondition', 'jqlerrormsg-create');
    });

    function initJqlFieldDelay(conditionId, jqlerrormsgId) {
        setTimeout(function () {
            if( $("#" + conditionId) && $("#" + conditionId).length > 0 ) {
                initJQLField(conditionId, jqlerrormsgId);
            } else {
                initJqlFieldDelay(conditionId, jqlerrormsgId);
            }
        }, 100);
    }

    function initJQLField(conditionId, jqlerrormsgId) {
       alert("ddeed");

        var advSearch = $("#" + conditionId);
        var jqlFieldNames = JSON.parse($("#jqlFieldz").text());
        var jqlFunctionNames = JSON.parse($("#jqlFunctionNamez").text());
        var jqlReservedWords = JSON.parse($("#jqlReservedWordz").text());
        var jqlAutoComplete = JIRA.JQLAutoComplete({
            fieldID: advSearch.attr("id"),
            parser: JIRA.JQLAutoComplete.MyParser(jqlReservedWords),
            queryDelay: .65,
            jqlFieldNames: jqlFieldNames,
            jqlFunctionNames: jqlFunctionNames,
            minQueryLength: 0,
            allowArrowCarousel: true,
            autoSelectFirst: false,
            errorID: jqlerrormsgId
        });

        $("#" + jqlerrormsgId).show();
        advSearch.expandOnInput();
        jqlAutoComplete.buildResponseContainer();
        jqlAutoComplete.parse(advSearch.text());
        jqlAutoComplete.updateColumnLineCount();

        advSearch.keypress(function (event) {
            if( jqlAutoComplete.dropdownController === null || !jqlAutoComplete.dropdownController.displayed || jqlAutoComplete.selectedIndex < 0 ) {
                if( event.keyCode == 13 && !event.ctrlKey && !event.shiftKey ) {
                    event.preventDefault();
                    jqlAutoComplete.dropdownController.hideDropdown();
                }
            }
        }).bind('expandedOnInput',function () {
                jqlAutoComplete.positionResponseContainer();
            }).click(function () {
                jqlAutoComplete.dropdownController.hideDropdown();
            }).bind('webhooks.valueChanged', function () {
                jqlAutoComplete.parse(advSearch.val());
            });
    }
})(AJS.$);
