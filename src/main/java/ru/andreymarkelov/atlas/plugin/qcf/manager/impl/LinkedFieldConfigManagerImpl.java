package ru.andreymarkelov.atlas.plugin.qcf.manager.impl;

import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.apache.log4j.Logger;
import ru.andreymarkelov.atlas.plugin.qcf.manager.LinkedFieldConfigManager;
import ru.andreymarkelov.atlas.plugin.qcf.model.LinkedFieldData;

public class LinkedFieldConfigManagerImpl implements LinkedFieldConfigManager {
    private static final Logger logger = Logger.getLogger(LinkedFieldConfigManagerImpl.class);

    private static final String PLUGIN_KEY = "AM_WORKS_QCF_LINKED_FIELD";

    private final PluginSettings pluginSettings;

    public LinkedFieldConfigManagerImpl(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createSettingsForKey(PLUGIN_KEY);
    }

    @Override
    public LinkedFieldData get(FieldConfig fieldConfig) {
        Object obj = getPluginSettings().get(buildKey(fieldConfig));
        if (obj != null) {
            return stringToLinkedFieldData(obj.toString());
        }
        return null;
    }

    @Override
    public void set(FieldConfig fieldConfig, LinkedFieldData linkedFieldData) {
        getPluginSettings().put(buildKey(fieldConfig), linkedFieldDataToString(linkedFieldData));
    }

    private String buildKey(FieldConfig config) {
        return config.getFieldId().concat("_").concat(config.getId().toString());
    }

    private synchronized PluginSettings getPluginSettings() {
        return pluginSettings;
    }

    private LinkedFieldData stringToLinkedFieldData(String string) {
        try {
            JSONObject jsonObject = new JSONObject(string);
            JSONArray jsonArray = jsonObject.has("displayFields") ? jsonObject.getJSONArray("displayFields") : new JSONArray();
            String[] displayFields = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                displayFields[i] = jsonArray.getString(i);
            }
            return new LinkedFieldData(jsonObject.getString("jql"), displayFields);
        } catch (JSONException e) {
            logger.error("Error serialize linked field data", e);
            throw new RuntimeException("Error serialize linked field data", e);
        }
    }

    private String linkedFieldDataToString(LinkedFieldData linkedFieldData) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("jql", linkedFieldData.getJql());
            jsonObject.put("displayFields", linkedFieldData.getDisplayFields());
        } catch (JSONException e) {
            logger.error("Error serialize linked field data", e);
            throw new RuntimeException("Error serialize linked field data", e);
        }
        return jsonObject.toString();
    }
}
