package ru.andreymarkelov.atlas.plugin.qcf.action.field;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import ru.andreymarkelov.atlas.plugin.qcf.manager.LinkerMultiFieldConfigManager;

public class LinkerMultiFieldConfigAction extends BaseQueryFieldConfigAction {
    private final LinkerMultiFieldConfigManager linkerMultiFieldConfigManager;

    private String epicCalculateMode;

    public LinkerMultiFieldConfigAction(
            ManagedConfigurationItemService managedConfigurationItemService,
            AutoCompleteJsonGenerator autoCompleteJsonGenerator,
            LinkerMultiFieldConfigManager linkerMultiFieldConfigManager) {
        super(managedConfigurationItemService, autoCompleteJsonGenerator);
        this.linkerMultiFieldConfigManager = linkerMultiFieldConfigManager;
    }

    @Override
    public String doDefault() throws Exception {
        //epicCalculateMode = progressTrackerFieldConfigManager.getEpicCalculateMode(getFieldConfig()).toString();
        return INPUT;
    }

    @Override
    @com.atlassian.jira.security.xsrf.RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (!hasGlobalPermission(GlobalPermissionKey.ADMINISTER)) {
            return "securitybreach";
        }

        //progressTrackerFieldConfigManager.setEpicCalculateMode(getFieldConfig(), EpicCalculateMode.valueOf(epicCalculateMode));
        return getRedirect("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + getFieldConfig().getCustomField().getIdAsLong().toString());
    }

    public String getEpicCalculateMode() {
        return epicCalculateMode;
    }

    public void setEpicCalculateMode(String epicCalculateMode) {
        this.epicCalculateMode = epicCalculateMode;
    }
}
