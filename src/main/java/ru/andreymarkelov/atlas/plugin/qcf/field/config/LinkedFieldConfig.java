package ru.andreymarkelov.atlas.plugin.qcf.field.config;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import ru.andreymarkelov.atlas.plugin.qcf.manager.LinkedFieldConfigManager;
import ru.andreymarkelov.atlas.plugin.qcf.model.LinkedFieldData;

public class LinkedFieldConfig implements FieldConfigItemType {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final LinkedFieldConfigManager linkedFieldConfigManager;

    public LinkedFieldConfig(
            JiraAuthenticationContext jiraAuthenticationContext,
            LinkedFieldConfigManager linkedFieldConfigManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.linkedFieldConfigManager = linkedFieldConfigManager;
    }

    @Override
    public String getDisplayName() {
        return jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugin.qcf.field.linkedfield.config.edit");
    }

    @Override
    public String getDisplayNameKey() {
        return "ru.andreymarkelov.atlas.plugin.qcf.field.linkedfield.config.edit";
    }

    @Override
    public String getViewHtml(FieldConfig fieldConfig, FieldLayoutItem fieldLayoutItem) {
        LinkedFieldData linkedFieldData = linkedFieldConfigManager.get(fieldConfig);
        if (linkedFieldData != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("\n");
            stringBuilder.append(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugin.qcf.field.linkedfield.config.query")).append(": ").append(linkedFieldData.getJql()).append("\n");
            stringBuilder.append(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugin.qcf.field.linkedfield.config.options")).append(": ").append(linkedFieldData.getDisplayFields());
            return stringBuilder.toString();
        }
        return "";
    }

    @Override
    public String getObjectKey() {
        return "LinkedFieldConfigAction";
    }

    @Override
    public Object getConfigurationObject(Issue issue, FieldConfig fieldConfig) {
        return linkedFieldConfigManager.get(fieldConfig);
    }

    @Override
    public String getBaseEditUrl() {
        return "LinkedFieldConfigAction!default.jspa";
    }
}
