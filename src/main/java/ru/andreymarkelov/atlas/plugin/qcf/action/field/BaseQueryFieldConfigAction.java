package ru.andreymarkelov.atlas.plugin.qcf.action.field;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.web.action.admin.customfields.AbstractEditConfigurationItemAction;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;

abstract class BaseQueryFieldConfigAction extends AbstractEditConfigurationItemAction {
    private final AutoCompleteJsonGenerator autoCompleteJsonGenerator;

    public BaseQueryFieldConfigAction(
            ManagedConfigurationItemService managedConfigurationItemService,
            AutoCompleteJsonGenerator autoCompleteJsonGenerator) {
        super(managedConfigurationItemService);
        this.autoCompleteJsonGenerator = autoCompleteJsonGenerator;
    }

    // Utils for JQL
    public String getVisibleFieldNamesJson() throws JSONException {
        return autoCompleteJsonGenerator.getVisibleFieldNamesJson(getLoggedInUser(), getLocale());
    }

    public String getVisibleFunctionNamesJson() throws JSONException {
        return autoCompleteJsonGenerator.getVisibleFunctionNamesJson(getLoggedInUser(), getLocale());
    }

    public String getJqlReservedWordsJson() throws JSONException {
        return autoCompleteJsonGenerator.getJqlReservedWordsJson();
    }
}
